import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './src/HomeScreen';
import ChatScreen from './src/ChatScreen';

const Stack = createStackNavigator();

/**
 * A demonstration app for connecting to Seamly web
 * This app uses AsyncStorage and React Native Web View
 * to demonstrate a connection between RN and Seamly web.
 *
 * When using other components for storage or web rendering,
 * please refer to its documentation for the appropriate ways
 * of transferring and storing data.
 *
 * The actual chat implementation can be found in the ChatScreen file.
 */
function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Chat" component={ChatScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
