import React, {Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {WebView} from 'react-native-webview';

// Connect to your hosted web page here
const CHAT_URI = 'https://developers.seamly.ai/clients/web-ui/demos/app/index.html';

export default class ChatScreen extends Component {
  state = {
    loaded: false,
    preloadedData: null,
  };

  componentDidMount() {
    this.preloadData();
  }

  preloadData = async () => {
    let value;
    try {
      // This demo app uses AsyncStorage as a simple data storage mechanism.
      // You can connect to your own preferred data storage here.
      value = await AsyncStorage.getItem('@SeamlyBridge');
    } catch (e) {
      // If an error occurs we don't do anything as the chat will simply start from scratch
      console.error("Couldn't retrieve data from storage: ", e);
    }
    // If you want to make changes, be sure be sure to keep the naming ('window.seamlyBridge') below intact
    this.setState({
      loaded: true,
      preloadedData: value ? 'window.seamlyBridgeData=' + value : null,
    });
  };

  // The data storage part of the bridge.
  onMessageReceived = (event) => {
    if (event.nativeEvent?.data) {
      // Optionally change this line when using your own data storage.
      // Make sure to only store the data object and store it as-is
      AsyncStorage.setItem('@SeamlyBridge', event.nativeEvent.data);
      this.webView.injectJavaScript(
        `window.seamlyBridgeData=${event.nativeEvent.data}`,
      );
    }
  };

  render() {
    if (!this.state.loaded) {
      // Since the JavaScript should be loaded before the webview is initialised,
      // we simply won't render until data is preloaded.
      return null;
    }
    return (
      <WebView
        ref={(r) => {
          this.webView = r;
        }}
        source={{uri: CHAT_URI}}
        injectedJavaScriptBeforeContentLoaded={this.state.preloadedData}
        onMessage={this.onMessageReceived}
      />
    );
  }
}
